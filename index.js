
/*
	MongoDB Aggregation

	
	syntax:

	db.collections.aggregate(
		[
			{stage 1},
			{stage 2},
			{stage 3}
		]
	)
*/

	db.fruits.insertMany([
			{
				"name": "Apple",
				"color": "Red",
				"stock": 20,
				"price": 40,
				"onSale": true,
				"origin": ["Philippines", "US"]
			},
			{
				"name": "Banana",
				"color": "Yellow",
				"stock": 15,
				"price": 20,
				"onSale": true,
				"origin": ["Philippines", "Ecuador"]
			},
			{
				"name": "Kiwi",
				"color": "Green",
				"stock": 25,
				"price": 50,
				"onSale": true,
				"origin": ["US", "China"]
			}, 
			{
				"name": "Mango",
				"color": "Yellow",
				"stock": 10,
				"price": 120,
				"onSale": false,
				"origin": ["Philippines", "India"]
			}
		]);

// Pipeline Stages

	// $match
		//Filters the documents to pass only the documents that match the specidied condition/s to the next pipeline stage

		// syntax: { $match: {field: value} }

	db.fruits.aggregate([
		{ $match: {"onSale": true} }
	])
	//using $match operator, find documents that are currently on sale.


	// $group
		// used to group elements together and field-value paris using the data from the grouped elements
	/*
		 syntax: 

		{ 
			$group: {
			 	_id: <expression>, 
			 	totalStock: { $sum: <expression> }
			}
		}
	*/

		db.fruits.aggregate(
			{ $match: {"onSale": true} },
			{ 
				$group: {
				 	_id: "$_id", 
				 	totalStock: { $sum: "$stocks" }
				}
			}
		)
		//group the documents according to id, and get the total of stocks. Assign these to property name _id and totalStocks



		//$project
			//reshapes each document in the stream, such as by adding new fields or removing existing fields


			//syntax: { project: {field: 0/1} }

		db.fruits.aggregate([
			{ $match: {"stock": {$gt: 10} } },
			{ $project: {"_id": 0} }
			// { $project: {"_id": 0, "name": 1, "stock": 1} }
				//if name & stock fields only
		])
		//look for documents that has stock greater than 10, return found documents without id



		// $sort
			// Reorders the document stream by a specified sort key. 

			// syntax: { $sort: {field: 1/-1} }

		db.fruits.aggregate([
			{ $match: {"stock": {$gt: 10} } },
			{ $project: {"_id": 0, "name": 1, "stock": 1} },
			{ $sort: {"stock": 1 } }
		])
		//on the 3rd stage, return the documents in ascending order



		// $count
			// Returns a count of the number of documents at this stage of the aggregation pipeline.

			// syntax: { $count: <stringName> }


		db.fruits.aggregate([
			{ $match: {"price": {$gte: 50} } },
			{ $count: "fruitCount" }
		])
		//look for documents that has a price of greater than or equal to 50, return the count of the number of documents and assign to property name fruitCount



		// $unwind
			// Deconstructs an array field from the input documents to output a document for each element. 
			// Each output document replaces the array with an element value. 

			// syntax: { $unwind: <field path> }

		db.fruits.aggregate([
			{ $unwind: "$origin"}
		])




		db.fruits.aggregate([
			{ $unwind: "$origin"},
			{ $group: {_id: "$origin", kinds: {$sum: 1} } }
		])
		//2nd stage, group documents according to their origin and count each document to 1 using sum operator and assign it to property name kinds